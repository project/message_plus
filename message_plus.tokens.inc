<?php

/**
 * @file
 * Provides Token integration for Message Plus.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\message\MessageInterface;

/**
 * Implements hook_token_info().
 */
function message_plus_token_info() {

  $entity_type_manager = \Drupal::service('entity_type.manager');
  $entity_definitions = $entity_type_manager->getDefinitions();

  $message_tokens = [];

  foreach ($entity_definitions as $entity_name => $entity_definition) {
    if ($entity_definition->getGroup() !== 'content' || $entity_definition->id() === 'message') {
      continue;
    }

    $message_tokens["{$entity_name}_name"] = [
      'name' => t('@entity_type name', [
        '@entity_type' => $entity_definition->getLabel(),
      ]),
    ];
    $message_tokens["{$entity_name}_link"] = [
      'name' => t('@entity_type link', [
        '@entity_type' => $entity_definition->getLabel(),
      ]),
    ];
  }

  $message_tokens['author_link'] = [
    'name' => t('Author link'),
  ];

  return [
    'tokens' => [
      'message' => $message_tokens,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function message_plus_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();
  $replacements = [];

  if ($type != 'message' || !($message = $data['message'] ?? NULL) instanceof MessageInterface) {
    return [];
  }

  foreach ($tokens as $name => $original) {
    if ($name === 'author_link') {
      $replacements[$original] = $message->getOwner()->toLink()->toString();
      continue;
    }

    // Process reference field tokens.
    $matches = [];
    preg_match('/(?<=field_)(.*)(?=_reference)/', $name, $matches);
    $entity_type_id = $matches[0] ?? NULL;

    if (
      $entity_type_id &&
      ($field_name = 'field_' . $entity_type_id . '_reference') &&
      $message->hasField($field_name) &&
      $entity_tokens = $token_service->findWithPrefix($tokens, $field_name)
    ) {
      $replacements += $token_service->generate($entity_type_id, $entity_tokens, [
        $entity_type_id => $message->get($field_name)->entity,
      ], $options, $bubbleable_metadata);
    }

    // Process reference field name and link.
    foreach (['name', 'link'] as $string_ending) {
      if (!str_ends_with($name, $string_ending)) {
        continue;
      }
      $entity_type_id = mb_substr($name, 0, -5);
      $field_name = 'field_' . $entity_type_id . '_reference';
      if ($message->hasField($field_name) && $entity = $message->get($field_name)->entity) {
        switch ($name) {
          case "{$entity_type_id}_name":
            $replacements[$original] = $entity->label();
            break;

          case "{$entity_type_id}_link":
            $replacements[$original] = $entity->toLink()->toString();
            break;
        }
      }
    }
  }

  return $replacements;
}
