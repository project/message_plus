<?php

namespace Drupal\message_plus\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Message plus settings for this site.
 */
class EntityTemplateSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected readonly EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');

    return $instance;
  }

  /**
   * List of message templates.
   *
   * @var array
   */
  protected array $templates = [];

  const CONFIG_NAME = 'message_plus.entity_type_message_template';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'message_plus_entity_template_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $config = $this->config(static::CONFIG_NAME);
    $entity_definitions = $this->entityTypeManager->getDefinitions();

    $form['entity_types'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity types'),
    ];

    $actions = [
      'insert' => $this->t('Insert'),
      'update' => $this->t('Update'),
      'delete' => $this->t('Delete'),
    ];

    $message_templates = $this->entityTypeManager
      ->getStorage('message_template')
      ->loadMultiple();

    $options = [NULL => $this->t('-None-')] + array_map(fn($template) => $template->label(), $message_templates);

    foreach ($entity_definitions as $entity_name => $entity_definition) {
      if ($entity_definition->getGroup() !== 'content' || $entity_definition->id() === 'message') {
        continue;
      }

      $form['entity_types'][$entity_name] = [
        '#type' => 'details',
        '#title' => $entity_definition->getLabel(),
        '#open' => $config->get($entity_name),
      ];

      $bundles_info = $this->entityTypeBundleInfo->getBundleInfo($entity_name);
      foreach ($bundles_info as $bundle => $bundle_info) {
        $form['entity_types'][$entity_name][$bundle] = [
          '#type' => 'details',
          '#title' => $bundle_info['label'] ?? $bundle,
          '#open' => FALSE,
        ];
        if ($config->get("$entity_name.$bundle")) {
          $form['entity_types'][$entity_name][$bundle]['#title'] .= ' ' . $this->t('(Selected)');
        }
        foreach ($actions as $action => $action_label) {
          $form['entity_types'][$entity_name][$bundle][$action] = [
            '#type' => 'select',
            '#title' => $action_label,
            '#options' => $options,
            '#default_value' => $config->get("$entity_name.$bundle.$action") ?? [],
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);
    $values = [];
    foreach ($form_state->getValue('entity_types') as $entity_type => $bundles) {
      foreach ($bundles as $bundle => $actions) {
        foreach ($actions as $action => $template) {
          if (!empty($template)) {
            $this->addFieldToMessageTemplate($entity_type, $template);
            $values[$entity_type][$bundle][$action] = $template;
          }
        }
      }
    }
    $config->setData($values);
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Add reference field to message template if needed.
   *
   * @param string $entity_type
   *   Reference entity_type.
   * @param string $template_id
   *   Message template id.
   */
  protected function addFieldToMessageTemplate($entity_type, $template_id) {
    $message_entity_type_id = 'message';
    $message_template_entity_type_id = 'message_template';
    $template = $this->templates[$template_id] ??= $this->entityTypeManager
      ->getStorage($message_template_entity_type_id)
      ->load($template_id);
    if (!$template) {
      return;
    }

    $field_name = 'field_' . $entity_type . '_reference';

    // Add or remove the body field, as needed.
    $field_storage = FieldStorageConfig::loadByName($message_entity_type_id, $field_name);
    if (empty($field_storage)) {
      $field_storage = FieldStorageConfig::create([
        'field_name' => $field_name,
        'entity_type' => $message_entity_type_id,
        'type' => 'entity_reference',
        'cardinality' => 1,
        'settings' => [
          'target_type' => $entity_type,
        ],
      ]);
      $field_storage->save();
    }

    $field = FieldConfig::loadByName($message_entity_type_id, $template_id, $field_name);
    if (empty($field)) {
      $field = FieldConfig::create([
        'field_name' => $field_name,
        'label' => $this->t('Relation to @entity_type', ['@entity_type' => $entity_type]),
        'entity_type' => $message_entity_type_id,
        'field_storage' => $field_storage,
        'bundle' => $template_id,
      ]);
      $field->save();
    }
  }

}
