<?php

namespace Drupal\message_plus\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use Drupal\message\Entity\Message;
use Drupal\message\Entity\MessageTemplate;
use Drupal\message\MessageInterface;
use Drupal\message_notify\MessageNotifier;
use Drupal\message_plus\Form\EntityTemplateSettingsForm;
use Drupal\message_plus\MessageRecipientsPluginManager;

/**
 * Message helper.
 */
class MessageProvider {

  use StringTranslationTrait;

  /**
   * Name of message sender queue worker.
   */
  const MESSAGE_QUEUE_WORKER = 'message_plus_worker';

  /**
   * Queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected readonly QueueInterface $queue;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected readonly LoggerChannelInterface $logger;

  /**
   * Static cache for message template entities.
   *
   * @var array
   */
  protected array $templates = [];

  /**
   * MessageProvider constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Manager.
   * @param \Drupal\message_notify\MessageNotifier $notifier
   *   The notifier plugin manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_channel_factory
   *   Logger channel factory.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\message_plus\MessageRecipientsPluginManager $messageRecipientsManager
   *   The recipients message plugin manager.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly MessageNotifier $notifier,
    protected readonly AccountInterface $currentUser,
    LoggerChannelFactory $logger_channel_factory,
    QueueFactory $queue_factory,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly MessageRecipientsPluginManager $messageRecipientsManager
  ) {
    $this->queue = $queue_factory->get(static::MESSAGE_QUEUE_WORKER);
    $this->logger = $logger_channel_factory->get('message_plus');
  }

  /**
   * Send or queue message on entity insert/update/delete.
   *
   * @param string $action
   *   Action type.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity or entity_id that is associated with message.
   * @param array $recipients
   *   (optional) Recipients.
   * @param array $additional_fields
   *   (optional) Additional fields to set into message.
   * @param bool|null $use_queue
   *   (optional) Override default queue configuration.
   *   If TRUE then force send via queue, otherwise send immediately.
   */
  public function processEntityActionNotifications($action, ContentEntityInterface $entity, array $recipients = [], array $additional_fields = [], bool $use_queue = NULL) {
    $template_id = $this->configFactory
      ->get(EntityTemplateSettingsForm::CONFIG_NAME)
      ->get("{$entity->getEntityTypeId()}.{$entity->bundle()}.$action");

    // Check if template exist in settings and
    // if entity was changed (only for update action).
    if ($template_id && ($action !== 'update' || $this->entityWasUpdated($entity))) {
      $this->processNotifications($template_id, $entity, $additional_fields, $recipients, $use_queue);
    }
  }

  /**
   * Send or queue message depends on message template configuration.
   *
   * @param string $template_id
   *   Temlpate id.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity that is associated with message.
   * @param array $recipients
   *   (optional) Recipients.
   * @param array $additional_fields
   *   (optional) Additional fields to set into message.
   * @param bool|null $use_queue
   *   (optional) Override default queue configuration.
   *   If TRUE then force send via queue, otherwise send immediately.
   */
  public function processNotifications($template_id, ContentEntityInterface $entity, array $recipients = [], array $additional_fields = [], bool $use_queue = NULL) {
    if (!$template_id || !($template = $this->getTemplate($template_id))) {
      $this->logger->error($this->t('The @template_id template does not exist', [
        '@template_id' => $template_id,
      ]));
      return;
    }

    $process_via_queue = $use_queue ?? $template->getThirdPartySetting('message_plus', 'queue');

    $message = $this->createMessage($template_id, $entity, $recipients, $additional_fields);

    // Skip sending process if there are no senders or recipients.
    $senders = $template->getThirdPartySetting('message_plus', 'sender_type');
    if (empty($senders) || $message->get('recipients')->isEmpty()) {
      return;
    }

    if ($process_via_queue) {
      $this->queueNotification($message);
    }
    else {
      $this->sendNotification($message);
    }
  }

  /**
   * Process message via queue.
   *
   * @param \Drupal\message\MessageInterface $message
   *   Message to queue.
   */
  public function queueNotification(MessageInterface $message) {
    $this->queue->createItem([
      'message_id' => $message->id(),
    ]);
  }

  /**
   * Send notifications.
   *
   * @param \Drupal\message\MessageInterface $message
   *   Message entity to send.
   */
  public function sendNotification(MessageInterface $message) {
    /** @var \Drupal\user\UserInterface[] $recipients */
    $recipients = $message->get('recipients')->referencedEntities();
    if (empty($recipients)) {
      return;
    }

    $senders = $message->getTemplate()->getThirdPartySetting('message_plus', 'sender_type');
    foreach ($recipients as $recipient) {
      foreach ($senders as $sender) {
        try {
          $this->doSendNotification($message, $sender, ['mail' => $recipient->getEmail()]);
        }
        catch (\Throwable $e) {
          $this->logger->error($this->t('Encountered error during sending message for the user with email - @email, message id is - @message_id, via email.</br> Error: @error <br /> @trace', [
            '@email' => $recipient->label(),
            '@message_id' => $message->id(),
            '@error' => $e->getMessage(),
            '@trace' => Error::formatBacktrace($e->getTrace()),
          ]));
        }
      }
    }
  }

  /**
   * Create new message.
   *
   * @param string $template_id
   *   Template id.
   * @param mixed $entity
   *   Entity that is associated with message.
   * @param \Drupal\user\UserInterface[] $recipients
   *   List of user to notify.
   * @param array $additional_fields
   *   (optional) Additional fields.
   *
   * @return \Drupal\message\MessageInterface
   *   Object with new created message entity.
   */
  public function createMessage(string $template_id, $entity, array $recipients, array $additional_fields = []) {
    $message = Message::create([
      'template' => $template_id,
      'uid' => $this->currentUser->id(),
      'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
    ]);

    $field_name = 'field_' . $entity->getEntityTypeId() . '_reference';
    if ($message->hasField($field_name)) {
      $message->set($field_name, $entity);
    }

    $arguments = ['@entity_name' => $entity->label()];

    foreach ($additional_fields as $field => $value) {
      // Provide ability to set arguments.
      if (str_starts_with($field, '@')) {
        $arguments[$field] = $value;
        continue;
      }

      $message->set($field, $value);
    }

    // @todo Implement arguments alter here.
    $message->setArguments($message->getArguments() + $arguments);

    // Set recipients.
    $recipients = $recipients ?: $this->getDefaultRecipients($message, $entity);
    foreach ($recipients as $recipient) {
      $message->get('recipients')->appendItem($recipient);
    }

    $message->save();

    return $message;
  }

  /**
   * Check if message template exist.
   *
   * @param string $template_id
   *   Message template id.
   *
   * @return \Drupal\message\Entity\MessageTemplate|null
   *   <Message template entity or nothing.
   */
  protected function getTemplate($template_id): ?MessageTemplate {
    return $this->templates[$template_id]
      ??= $this->entityTypeManager->getStorage('message_template')->load($template_id);
  }

  /**
   * Get list of recipients.
   *
   * @param \Drupal\message\MessageInterface $message
   *   Message from which fetch list of recipients.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity that is associated with message.
   *
   * @return array
   *   List of emails.
   */
  protected function getDefaultRecipients(MessageInterface $message, ContentEntityInterface $entity) {
    $recipients = [];
    $template_recipients = $message->getTemplate()
      ->getThirdPartySetting('message_plus', 'recipients', []);

    if (
      !empty($template_recipients['plugin_id']) &&
      !empty($template_recipients['configuration']) &&
      $recipients_plugin = $this->messageRecipientsManager
        ->createInstance($template_recipients['plugin_id'], $template_recipients['configuration'])
    ) {
      $recipients = $recipients_plugin->getRecipients($entity);
    }

    return $recipients;
  }

  /**
   * Send notification via notifier.
   *
   * @param \Drupal\message\MessageInterface $message
   *   Message.
   * @param string $notifier_id
   *   Notifier id.
   * @param array $options
   *   Options.
   *
   * @return bool
   *   Status of message.
   */
  protected function doSendNotification(MessageInterface $message, string $notifier_id, array $options = []): bool {
    $options += [
      'save on success' => FALSE,
    ];
    return $this->notifier->send($message, $options, $notifier_id);
  }

  /**
   * Check if entity was updated.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Updated entity.
   *
   * @return bool
   *   True if entity was updated.
   */
  protected function entityWasUpdated(ContentEntityInterface $entity): bool {
    // Check if the original exists - just in case.
    if (!$original = $entity->original) {
      return FALSE;
    }

    foreach ($entity->getFieldDefinitions() as $field_definition) {
      if ($this->hasFieldValueChanged($field_definition, $entity, $original)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Checks whether the field values changed compared to the original entity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition of field to compare for changes.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to check for field changes.
   * @param \Drupal\Core\Entity\ContentEntityInterface $original
   *   Original entity to compare against.
   *
   * @return bool
   *   True if the field value changed from the original entity.
   */
  protected function hasFieldValueChanged(FieldDefinitionInterface $field_definition, ContentEntityInterface $entity, ContentEntityInterface $original): bool {
    $field_name = $field_definition->getName();
    $langcodes = array_keys($entity->getTranslationLanguages());
    if ($langcodes !== array_keys($original->getTranslationLanguages())) {
      // If the list of langcodes has changed, we need to save.
      return TRUE;
    }
    foreach ($langcodes as $langcode) {
      $items = $entity->getTranslation($langcode)->get($field_name)->filterEmptyItems();
      $original_items = $original->getTranslation($langcode)->get($field_name)->filterEmptyItems();
      // If the field items are not equal, we need to save.
      if (!$items->equals($original_items)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
