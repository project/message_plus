<?php

declare(strict_types = 1);

namespace Drupal\message_plus;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\message_plus\Annotation\MessageRecipients;

/**
 * MessageRecipients plugin manager.
 */
final class MessageRecipientsPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/MessageRecipients', $namespaces, $module_handler, MessageRecipientsInterface::class, MessageRecipients::class);
    $this->alterInfo('message_recipients_info');
    $this->setCacheBackend($cache_backend, 'message_recipients_plugins');
  }

}
