<?php

declare(strict_types = 1);

namespace Drupal\message_plus;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for message_recipients plugins.
 */
interface MessageRecipientsInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Get list of recipients.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity object.
   *
   * @return array
   *   List of recipients.
   */
  public function getRecipients(ContentEntityInterface $entity): array;

  /**
   * Get default configurations.
   *
   * @return array
   *   List of configurations.
   */
  public function defaultConfigurations(): array;

  /**
   * Build configuration form.
   *
   * @param array $form
   *   Form associated array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Recipient form array.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array;

}
