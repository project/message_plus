<?php

declare(strict_types = 1);

namespace Drupal\message_plus\Plugin\MessageRecipients;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\message_plus\MessageRecipientsPluginBase;
use Drupal\user\EntityOwnerInterface;

/**
 * Plugin implementation of the message_recipients.
 *
 * @MessageRecipients(
 *   id = "entity_owner",
 *   label = @Translation("Entity owner")
 * )
 */
class IssueCommentGroupMembers extends MessageRecipientsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getRecipients(ContentEntityInterface $entity): array {
    if ($entity instanceof EntityOwnerInterface) {
      return [$entity->getOwner()];
    }

    return [];
  }

}
