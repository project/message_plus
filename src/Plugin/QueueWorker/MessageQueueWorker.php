<?php

namespace Drupal\message_plus\Plugin\QueueWorker;

use Drupal\message_plus\Service\MessageProvider;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process a queue sending message using queue.
 *
 * @QueueWorker(
 *   id = "message_plus_worker",
 *   title = @Translation("Message sender"),
 *   cron = {"time" = 120}
 * )
 */
class MessageQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Message service.
   *
   * @var \Drupal\message_plus\Service\MessageProvider
   */
  protected $messageService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MessageProvider $message_service, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->messageService = $message_service;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('message_plus.message_provider'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data) {
    if (!empty($data)) {
      /** @var \Drupal\message\MessageInterface $message */
      $message = $this->entityTypeManager
        ->getStorage('message')
        ->load($data['message_id']);

      // Potentially message can be deleted.
      if (!$message) {
        return;
      }

      $this->messageService->sendNotification($message);
    }
  }

}
